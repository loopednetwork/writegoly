# WriteGoly

Golang CLI/TUI client for [Write Freely](https://writefreely.org/).

## Getting Started

This project will be a CLI (directly accessed from the command line with parameters) and TUI (accessed from the command line but interactively) client for the [Write.as](https://developers.write.as/docs/api/#introduction) API. The initial focus will be on getting the CLI functionality down, with the TUI functionality coming later.

## Initial Goals

Initial goals are all based on CLI functionality:

- Login
- Read authentication information from config file
- Allow for post creation
- Logout

## Later Goals

Later goals are mainly focused on TUI functionality:

- TUI options for all of the above.
- TUI with something like [Charm](https://charm.sh/) so that it looks nice.
- Post deletion/editing
- GitLab Actions to compile releases for common operating systems.

## Caveats

All initial development will be focused with Linux as the underlying OS. Later, I'll attempt to make it agnostic.


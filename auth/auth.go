package auth

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"time"
)

type ConfigData struct {
	InstanceDomain string `json:"instance_domain"`
	AccessToken    string `json:"access_token"`
}
type AuthResponseData struct {
	Code int `json:"code"`
	Data struct {
		AccessToken string `json:"access_token"`
		User        struct {
			Username string    `json:"username"`
			Email    string    `json:"email"`
			Created  time.Time `json:"created"`
		} `json:"user"`
	} `json:"data"`
}
type AuthBody struct {
	Alias    string `json:"alias"`
	Password string `json:"pass"`
}

func CheckFileExists(filePath string) bool {
	_, error := os.Stat(filePath)
	return !errors.Is(error, os.ErrNotExist)
}

func GetConfigPath() string {
	// Will need to be changed later since this only works for Linux.
	currentUser, err := user.Current()
	if err != nil {
		fmt.Printf("Error getting current user: %v\n", err)
		os.Exit(1)
	}
	return fmt.Sprintf("/home/%s/.config/writegoly/config.json", currentUser.Username)
}

func DeleteConfigFile(filePath string) {
	// Accepts a file path and removes the offending file if it exists.
	if CheckFileExists(filePath) {
		err := os.Remove(filePath)
		if err != nil {
			fmt.Printf("Error removing config file: %v\n", err)
		}
	}
}

func PostAuth(url string, bodyContent []byte) string {
	/* Function to authenticate a user. It will return a string containing the
	access token. It creates its own HTTP client and uses that to POST the data
	to the user's Write Freely instance. */
	client := &http.Client{}
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(bodyContent))
	if err != nil {
		fmt.Printf("Error trying to create authentication request: %v\n", err)
		os.Exit(1)
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("Error POSTing the authentication request: %v\n", err)
		os.Exit(1)
	}

	// Read the response into the expected struct.
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("Error reading authentication response: %v\n", err)
		os.Exit(1)
	}
	var authResponse AuthResponseData
	err = json.Unmarshal(body, &authResponse)
	if err != nil {
		fmt.Printf("Error processing authentication response: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf("Body received from auth: %v\n", body)
	stringBody := string(body[:])
	fmt.Printf("Parsed body received from auth: %v\n", stringBody)
	fmt.Printf("Parsed auth response: %v\n", authResponse)
	return authResponse.Data.AccessToken
}

func AuthenticateFresh(instance string, userName string, password string) {
	/*
		Does a fresh authentication of the user. If content is currently at the
		configuration path, it is removed and overwritten.
	*/
	// Get the config path and delete the existing file if needed.
	configPath := GetConfigPath()
	DeleteConfigFile(configPath)

	// Create a struct with the user's authentication information.
	authData := AuthBody{userName, password}
	authDataJson, err := json.Marshal(authData)
	if err != nil {
		fmt.Printf("Error converting authentication data to JSON: %v\n", err)
		os.Exit(1)
	}

	// Create an HTTP client and authenticate.
	authUrl := fmt.Sprintf("https://%v/api/auth/login", instance)
	fmt.Printf("Using a login URL of: %v\n", authUrl)
	accessToken := PostAuth(authUrl, authDataJson)
	fmt.Printf("Received access token: %v\n", accessToken)

	// Create a config struct, make it JSON, and write the file.
	configDataObject := ConfigData{instance, accessToken}
	configJson, err := json.Marshal(configDataObject)
	if err != nil {
		fmt.Printf("Error marshalling configuration file: %v\n", err)
		os.Exit(1)
	}
	_ = ioutil.WriteFile(configPath, configJson, 0644)

	fmt.Println("Config file successfully updated!")
}

func AuthenticateUser() (string, string) {
	// Returns a string for the user's access token and a string for the domain
	// of the user's instance.

	// Put together the path for the config file.
	configPath := GetConfigPath()
	var instance string
	var token string

	// Check if a config file already exists.
	fileExists := CheckFileExists(configPath)
	if fileExists {
		// Load the file.
		content, err := ioutil.ReadFile(configPath)
		if err != nil {
			fmt.Printf("Error when opening the file at %v\n", configPath)
			fmt.Printf("Error was: %v\n", err)
			os.Exit(1)
		}
		var currentConfig ConfigData
		err = json.Unmarshal(content, &currentConfig)
		if err != nil {
			fmt.Printf("Error unmarshalling JSON: %v\n", err)
			os.Exit(1)
		}

		// Map the values to the variables to be returned.
		instance = currentConfig.InstanceDomain
		token = currentConfig.AccessToken
	} else {
		// Prompt the user for information.
		fmt.Println("No config file found! Use the login parameter!")
		os.Exit(1)
	}

	return instance, token
}

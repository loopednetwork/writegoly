package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/loopednetwork/writegoly/auth"
)

func PrintHelp() {
	// Function to show the help information. Used when no CLI arguments or
	// "help" is specified.
	fmt.Println("writegoly - CLI/TUI client for Write Freely instances.")
	fmt.Println("            Currently only CLI functionality is implemented.")
	fmt.Println("")
	fmt.Println("writegoly [help|login|logout|post]")
}

func main() {
	// Show the help menu if no arguments were given.
	if len(os.Args) == 1 {
		PrintHelp()
		os.Exit(0)
	}

	// Start the switch statement for the arguments.
	firstArg := strings.ToLower(os.Args[1])
	switch firstArg {
	case "login":
		// Check if additional arguments were specified.
		fmt.Println("You want to log in!")
		if len(os.Args) >= 5 {
			instance := os.Args[2]
			userName := os.Args[3]
			password := os.Args[4]
			auth.AuthenticateFresh(instance, userName, password)
		} else {
			fmt.Println("Use of \"login\" requires the instance, username, and password.")
			fmt.Println("Example: writegoly login write.as loopednetwork abc123")
			fmt.Println("\nWhere the following applies:")
			fmt.Println("\t\"write.as\" is the domain of the Write Freely instance.")
			fmt.Println("\t\"loopednetwork\" is the username.")
			fmt.Println("\t\"abc123\" is the password for the account.")
		}
	default:
		fmt.Printf("Unknown CLI parameter: %v\n", firstArg)
	}

	/*
		instance, accessToken := auth.AuthenticateUser()
		fmt.Printf("Access Token: %v\n", accessToken)
		fmt.Printf("Instance: %v\n", instance)
	*/
}
